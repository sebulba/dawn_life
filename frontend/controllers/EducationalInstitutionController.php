<?php

namespace frontend\controllers;

use frontend\models\EducationalInstitution;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EducationalInstitutionController extends Controller
{
    public function actionView($educInstName)
    {
        $model = $this->findModel($educInstName);

        return $this->render('view',['model' => $model]);
    }

    protected function findModel($educInstName)
    {
        $name = str_replace('_', ' ', $educInstName);

        if (($model = EducationalInstitution::findOne(['name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
