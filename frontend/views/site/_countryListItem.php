<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 25.06.16
 * Time: 5:13
 */
use yii\bootstrap\Html;
?>
<?= Html::a(
    Html::img($model->getImageFile(),[
        'class' => 'country-img'
    ]).Html::tag('p', $model->short_description,[
        'class' => 'country-img'
    ])
    ,$model->getUrl()
)
?>
