<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 25.06.16
 * Time: 5:13
 */
use yii\bootstrap\Html;
use frontend\models\Country;
use yii\helpers\Url;
?>
<?
$country = new Country();
$country_id = $country
    ->find()
    ->where([
        'name' => $this->context->actionParams['url']
    ])
    ->one()->id;
?>
<?if($model->getEducationalInstitutions()->where(['country_id' => $country_id])->all()){?>
<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse<?= $model->name?>"><?= $model->name?></a>
            </h4>
        </div>

        <div id="collapse<?= $model->name?>" class="panel-collapse collapse">
            <?
            foreach($model->getEducationalInstitutions()->where(['country_id' => $country_id])->all() as $institution)
            {
                $institution_name = str_replace(' ', '_', $institution->name);
                echo Html::tag(
                    'div',
                    Html::a(
                        $institution->name,
                        Url::toRoute(["/educational-institution/$institution_name"])),[
                    'class' => 'panel-body'
                    ]
                );
            }
            ?>
        </div>
    </div>
</div>
<?}?>
