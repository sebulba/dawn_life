<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use frontend\models\Country;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application 1';
?>
<div class="site-index">

    <div class="body-content">

        <?= $model->content ?>
        
    </div>
</div>
