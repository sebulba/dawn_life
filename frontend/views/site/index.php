<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use frontend\models\Country;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application 1';
?>
<div class="site-index">

    <div class="jumbotron main-img">
        <h1>Welcome!</h1>

        <h2 class="lead">STUDY ABROAD, COME TO EUROPE!</h2>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to("/fillBlank")?>">Get started with Dawn Life</a></p>
    </div>

    <div class="body-content">

        <?= $model->content ?>

        <?= ListView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Country::find(),
            ]),
            'itemOptions' => ['class' => 'col-lg-4 country-item'],
            'itemView' => '_countryListItem',
            'layout' => "{items}\n{pager}",
            'options' => ['class' => 'row country-wrap']
        ]) ?>

    </div>
</div>
