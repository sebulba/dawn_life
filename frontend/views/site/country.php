<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use frontend\models\Speciality;

/* @var $this yii\web\View */

$this->title = 'My Yii Application 1';
?>
<div class="site-index">

    <div class="body-content">

        <?= $model->text ?>

        <?= ListView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Speciality::find(),
            ]),
            'itemOptions' => ['class' => ''],
            'itemView' => '_specialityListItem',
            'layout' => "{items}\n{pager}",
            'options' => ['class' => ''],
        ]) ?>

    </div>
</div>
