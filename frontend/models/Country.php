<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "country".
 *
 * @property string $id
 * @property string $name
 * @property string $short_description
 * @property string $text
 * @property string $image
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_description', 'text', 'image'], 'required'],
            [['short_description', 'text', 'image'], 'string'],
            [['name'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'short_description' => 'Short Description',
            'text' => 'Text',
            'image' => 'Image',
        ];
    }

    public function getUrl()
    {
        return Url::to("/$this->name");
    }

    public function getImageFile()
    {
        return $this->image;
    }
}
