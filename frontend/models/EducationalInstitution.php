<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "educational_institution".
 *
 * @property string $id
 * @property string $name
 * @property string $courses
 * @property string $country_id
 * @property string $specialty_id
 *
 * @property Country $country
 * @property Speciality $specialty
 */
class EducationalInstitution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'educational_institution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'courses', 'country_id', 'specialty_id'], 'required'],
            [['courses'], 'string'],
            [['country_id', 'specialty_id'], 'integer'],
            [['name'], 'string', 'max' => 63],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['specialty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Speciality::className(), 'targetAttribute' => ['specialty_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'courses' => 'Courses',
            'country_id' => 'Country ID',
            'specialty_id' => 'Specialty ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialty()
    {
        return $this->hasOne(Speciality::className(), ['id' => 'specialty_id']);
    }
}
