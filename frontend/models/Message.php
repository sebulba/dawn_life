<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property string $id
 * @property string $email
 * @property string $name
 * @property string $message
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'message'], 'required'],
            [['message'], 'string'],
            [['name'], 'string', 'max' => 63],
            [['email'], 'email', 'message' => 'Your email address is not a valid, please enter your email in the format name@domen.com']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'message' => 'Message',
        ];
    }
}
