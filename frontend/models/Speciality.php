<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "speciality".
 *
 * @property string $id
 * @property string $name
 *
 * @property EducationalInstitution[] $educationalInstitutions
 */
class Speciality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'speciality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducationalInstitutions()
    {
        return $this->hasMany(EducationalInstitution::className(), ['specialty_id' => 'id']);
    }
}
