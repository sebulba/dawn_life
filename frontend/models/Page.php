<?php

namespace frontend\models;

use backend\models\Country;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "page".
 *
 * @property string $id
 * @property string $content
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $url
 */
class Page extends \yii\db\ActiveRecord
{
    const MAIN_PAGE_URL = 'site';
    const STUDY_COUNTRY_ID = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'meta_title', 'meta_description', 'meta_keywords', 'url'], 'required'],
            [['content'], 'string'],
            [['meta_title', 'meta_description', 'meta_keywords', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'url' => 'Url',
        ];
    }

    public function menuItems()
    {
        $rawiItemAr = $this->find()->orderBy('sort')->all();


        foreach($rawiItemAr as $key=>$menuItem)
        {
            if($menuItem->isStudyContryList())
                foreach($menuItem->isStudyContryList() as $country)
                {
                    $itemAr[$key]['items'][] = [
                        'label' => $country->name,
                        'url' => Url::to("/$country->name")
                    ];
                }
            $itemAr[$key]['label'] = $menuItem->meta_title;
            $itemAr[$key]['url'] = Url::to("/$menuItem->url");
        }
        return $itemAr;
    }

    public function isStudyContryList()
    {
        return $this->id == self::STUDY_COUNTRY_ID
            ? Country::find()->all()
            : null;
    }
}
