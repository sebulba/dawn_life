<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "app_form".
 *
 * @property string $id
 * @property string $name
 * @property string $surname
 * @property string $maiden_name
 * @property string $speciality
 * @property string $gender
 * @property string $birth_date
 * @property string $birth_place
 * @property string $father_name
 * @property string $mother_name
 * @property string $nationality
 * @property string $id_passport_number
 * @property string $visa_number
 * @property string $residence_card_number
 * @property string $p_street
 * @property string $p_village
 * @property string $p_post_code
 * @property string $p_city
 * @property string $p_country
 * @property string $phone_number
 * @property string $email
 * @property string $c_street
 * @property string $c_village
 * @property string $c_post_code
 * @property string $c_city
 * @property string $c_country
 * @property string $school_name
 * @property string $sch_city_and_country
 * @property string $certificate_number
 * @property string $sch_date_begin
 * @property string $sch_date_end
 * @property string $sch_graduation_year
 * @property string $collage_name
 * @property string $col_city_and_country
 * @property string $type_of_degree_awarded
 * @property string $courses
 * @property string $diploma_number
 * @property string $col_date_begin
 * @property string $col_date_end
 * @property string $col_graduation_year
 * @property string $reading
 * @property string $writing
 * @property string $speaking
 * @property string $test_name
 * @property string $grade
 * @property string $examination_date
 * @property string $friend_name
 * @property string $relationship
 * @property string $friend_phone_number
 * @property string $friend_email
 * @property string $friend_address
 * @property string $room
 *
 * @property Speciality $speciality0
 */
class AppForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'maiden_name', 'speciality', 'gender', 'birth_date', 'birth_place', 'father_name', 'mother_name', 'nationality', 'id_passport_number', 'visa_number', 'residence_card_number', 'p_street', 'p_village', 'p_post_code', 'p_city', 'p_country', 'phone_number', 'email', 'c_street', 'c_village', 'c_post_code', 'c_city', 'c_country', 'school_name', 'sch_city_and_country', 'certificate_number', 'sch_date_begin', 'sch_date_end', 'sch_graduation_year', 'collage_name', 'col_city_and_country', 'courses', 'diploma_number', 'col_date_begin', 'col_date_end', 'col_graduation_year', 'reading', 'writing', 'speaking', 'test_name', 'grade', 'examination_date', 'friend_name', 'relationship', 'friend_phone_number', 'friend_email', 'friend_address'], 'required'],
            [['speciality'], 'integer'],
            [['gender', 'type_of_degree_awarded', 'reading', 'writing', 'speaking', 'room'], 'string'],
            [['birth_date', 'sch_date_begin', 'sch_date_end', 'sch_graduation_year', 'col_date_begin', 'col_date_end', 'col_graduation_year', 'examination_date'], 'safe'],
            [['name', 'surname', 'maiden_name', 'nationality', 'id_passport_number', 'p_street', 'p_village', 'p_city', 'c_street', 'c_village', 'c_city', 'school_name', 'sch_city_and_country', 'collage_name', 'col_city_and_country', 'courses', 'diploma_number', 'test_name', 'friend_name', 'relationship'], 'string', 'max' => 63],
            [['birth_place', 'father_name', 'mother_name'], 'string', 'max' => 253],
            [['visa_number'], 'string', 'max' => 16],
            [['residence_card_number'], 'string', 'max' => 13],
            [['p_post_code', 'phone_number', 'c_post_code', 'grade', 'friend_phone_number'], 'string', 'max' => 15],
            [['p_country', 'c_country', 'certificate_number'], 'string', 'max' => 31],
            [['email', 'friend_email'], 'email'],
            [['friend_address'], 'string', 'max' => 127],
            [['speciality'], 'exist', 'skipOnError' => true, 'targetClass' => Speciality::className(), 'targetAttribute' => ['speciality' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'maiden_name' => 'Maiden Name',
            'speciality' => 'Speciality',
            'gender' => 'Gender',
            'birth_date' => 'Date of birth',
            'birth_place' => 'Place of birth',
            'father_name' => 'Father Name',
            'mother_name' => 'Mother Name',
            'nationality' => 'Nationality',
            'id_passport_number' => 'ID/PASSPORT NUMBER',
            'visa_number' => 'Visa Number',
            'residence_card_number' => 'Residence Card Number',
            'p_street' => 'Street',
            'p_village' => 'Village',
            'p_post_code' => 'Post Code',
            'p_city' => 'City',
            'p_country' => 'Country',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'c_street' => 'Street',
            'c_village' => 'Village',
            'c_post_code' => 'Post Code',
            'c_city' => 'City',
            'c_country' => 'Country',
            'school_name' => 'Name',
            'sch_city_and_country' => 'City/Town and Country',
            'certificate_number' => 'Certificate Number',
            'sch_date_begin' => 'Date of begin',
            'sch_date_end' => 'Date of end',
            'sch_graduation_year' => 'Year of graduation',
            'collage_name' => 'COLLEGE/UNIVERSITY NAME',
            'col_city_and_country' => 'City/Town and Country',
            'type_of_degree_awarded' => 'Type Of Degree Awarded',
            'courses' => 'PROGRAMMES/COURSES',
            'diploma_number' => 'Diploma Number',
            'col_date_begin' => 'Date of begin',
            'col_date_end' => 'Date of end',
            'col_graduation_year' => 'Year of graduation',
            'reading' => 'Reading',
            'writing' => 'Writing',
            'speaking' => 'Speaking',
            'test_name' => 'Test Name',
            'grade' => 'Grade',
            'examination_date' => 'Examination Date',
            'friend_name' => 'Name',
            'relationship' => 'Relationship',
            'friend_phone_number' => 'Phone Number',
            'friend_email' => 'Email',
            'friend_address' => 'Address',
            'room' => 'DO YOU INTEND TO APPLY FOR UNIVERSITY ACCOMMODATION?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeciality0()
    {
        return $this->hasOne(Speciality::className(), ['id' => 'speciality']);
    }
}
