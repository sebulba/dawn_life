<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "country".
 *
 * @property string $id
 * @property string $name
 * @property string $short_description
 * @property string $text
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_description', 'text'], 'required'],
            [['short_description', 'text'], 'string'],
            [['name'], 'string', 'max' => 63],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'short_description' => 'Short Description',
            'text' => 'Text',
            'image' => 'Image',
        ];
    }

    public function getImageFile()
    {
        return $this->image;
    }

    public function uploadImage($oldImage = null) {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        if(isset($oldImage))
            $this->image = $oldImage;

        $image = UploadedFile::getInstance($this, 'image');

        if (empty($image) && !isset($image)) {
            return false;
        }


        $ext = end((explode(".", $image->name)));

        $path = Yii::$app->params['uploadPath'] . $this->name . '.' .$ext;

        $this->image = $path;

        // the uploaded image instance
        return $image;
    }

    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        return true;
    }
}
