<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property string $id
 * @property string $email
 * @property string $name
 * @property string $message
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'message'], 'required'],
            [['message'], 'string'],
            [['email'], 'email'],
            [['name'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'message' => 'Text of message',
        ];
    }
}
