<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property string $id
 * @property string $content
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $url
 */
class Page extends \yii\db\ActiveRecord
{
    const CONTACT_ID = 4;
    const BLANK_ID = 5;
    const STUDY_COUNTRY_ID = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_title', 'meta_description', 'meta_keywords', 'url'], 'required'],
            ['content', 'required', 'when'  => function($this) {
                return $this->is_content_required() == 0;
            },'whenClient' => "function (attribute, value) {
                return false}"],
            [['content'], 'string'],
            [['sort'], 'integer'],
            [['meta_title', 'meta_description', 'meta_keywords', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'url' => 'Url',
            'sort' => 'Sorting Order',

        ];
    }

    public function is_content_required()
    {
        return $this->id == self::CONTACT_ID || $this->id == self::BLANK_ID || $this->id == self::STUDY_COUNTRY_ID;
    }
}
