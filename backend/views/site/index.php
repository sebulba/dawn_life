<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <ul>
        <li><a href="<?=Url::to(['page/'])?>">Pages</a></li>
        <li><a href="<?=Url::to(['country/'])?>">Countries</a></li>
        <li><a href="<?=Url::to(['speciality/'])?>">Speciality</a></li>
        <li><a href="<?=Url::to(['educational-institution/'])?>">Educational Institution</a></li>
        <li><a href="<?=Url::to(['message/'])?>">Message</a></li>
        <li><a href="<?=Url::to(['app-form/'])?>">Application Form</a></li>
    </ul>
</div>
