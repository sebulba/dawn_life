<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Country;
use backend\models\Speciality;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\EducationalInstitution */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educational-institution-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'courses')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(Country::find()->all(), 'id', 'name'))->label('Country') ?>

    <?= $form->field($model, 'specialty_id')->dropDownList(ArrayHelper::map(Speciality::find()->all(), 'id', 'name'))->label('Speciality') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
