<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\EducationalInstitution */

$this->title = 'Create Educational Institution';
$this->params['breadcrumbs'][] = ['label' => 'Educational Institutions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educational-institution-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
