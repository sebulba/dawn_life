<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'App Forms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'surname',
            'maiden_name',
            'speciality',
            // 'gender',
            // 'birth_date',
            // 'birth_place',
            // 'father_name',
            // 'mother_name',
            // 'nationality',
            // 'id_passport_number',
            // 'visa_number',
            // 'residence_card_number',
            // 'p_street',
            // 'p_village',
            // 'p_post_code',
            // 'p_city',
            // 'p_country',
            // 'phone_number',
            // 'email:email',
            // 'c_street',
            // 'c_village',
            // 'c_post_code',
            // 'c_city',
            // 'c_country',
            // 'school_name',
            // 'sch_city_and_country',
            // 'certificate_number',
            // 'sch_date_begin',
            // 'sch_date_end',
            // 'sch_graduation_year',
            // 'collage_name',
            // 'col_city_and_country',
            // 'type_of_degree_awarded',
            // 'courses',
            // 'diploma_number',
            // 'col_date_begin',
            // 'col_date_end',
            // 'col_graduation_year',
            // 'reading',
            // 'writing',
            // 'speaking',
            // 'test_name',
            // 'grade',
            // 'examination_date',
            // 'friend_name',
            // 'relationship',
            // 'friend_phone_number',
            // 'friend_email:email',
            // 'friend_address',
            // 'room',

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
