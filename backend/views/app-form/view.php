<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AppForm */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'App Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'maiden_name',
            'speciality',
            'gender',
            'birth_date',
            'birth_place',
            'father_name',
            'mother_name',
            'nationality',
            'id_passport_number',
            'visa_number',
            'residence_card_number',
            'p_street',
            'p_village',
            'p_post_code',
            'p_city',
            'p_country',
            'phone_number',
            'email:email',
            'c_street',
            'c_village',
            'c_post_code',
            'c_city',
            'c_country',
            'school_name',
            'sch_city_and_country',
            'certificate_number',
            'sch_date_begin',
            'sch_date_end',
            'sch_graduation_year',
            'collage_name',
            'col_city_and_country',
            'type_of_degree_awarded',
            'courses',
            'diploma_number',
            'col_date_begin',
            'col_date_end',
            'col_graduation_year',
            'reading',
            'writing',
            'speaking',
            'test_name',
            'grade',
            'examination_date',
            'friend_name',
            'relationship',
            'friend_phone_number',
            'friend_email:email',
            'friend_address',
            'room',
        ],
    ]) ?>

</div>
