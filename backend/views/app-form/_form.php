<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Speciality;

/* @var $this yii\web\View */
/* @var $model backend\models\AppForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12"><span>PERSONAL DETAILS :</span></div>
    </div>

    <div class="row">

        <div class="col-sm-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'maiden_name')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'speciality')->dropDownList(ArrayHelper::map(Speciality::find()->all(), 'id', 'name')) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'gender')->dropDownList([ 'male' => 'Male', 'female' => 'Female', ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'birth_date')->textInput() ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'birth_place')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'id_passport_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'visa_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'residence_card_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <span>PARENTS’ NAME :</span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'father_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'mother_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <span>PERMANENT ADDRESS :</span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'p_street')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'p_village')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'p_post_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'p_city')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'p_country')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <span>CORRESPONDENCE ADDRESS :</span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'c_street')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'c_village')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'c_post_code')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'c_city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'c_country')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12"><span>SECONDARY SCHOOL ATTENDED :</span></div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'school_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'sch_city_and_country')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'certificate_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'sch_date_begin')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'sch_date_end')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'sch_graduation_year')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12"><span>COLLEGE/UNIVERSITY ATTENDED :</span></div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'collage_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'col_city_and_country')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'type_of_degree_awarded')->dropDownList([ 'bachelor' => 'Bachelor', 'master' => 'Master', ], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'diploma_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'col_date_begin')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'col_date_end')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'courses')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'col_graduation_year')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12"><span>ENGLISH LANGUAGE SKILLS :</span></div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'reading')->dropDownList([ 'proficiency' => 'Proficiency', 'advanced' => 'Advanced', 'intermediate' => 'Intermediate', 'elementary' => 'Elementary', ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'writing')->dropDownList([ 'proficiency' => 'Proficiency', 'advanced' => 'Advanced', 'intermediate' => 'Intermediate', 'elementary' => 'Elementary', ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'speaking')->dropDownList([ 'proficiency' => 'Proficiency', 'advanced' => 'Advanced', 'intermediate' => 'Intermediate', 'elementary' => 'Elementary', ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12"><span>
                ENGLISH LANGUAGE CERTIFICATES :
            </span></div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'test_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'grade')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'examination_date')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <col-sm-12><span>
                PERSON CONTACT IN CASE OF AN EMERGENCY :
            </span></col-sm-12>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'friend_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'relationship')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'friend_phone_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="div">
        <div class="col-sm-6">
            <?= $form->field($model, 'friend_email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'friend_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'room')->dropDownList([ 'single' => 'Single', 'double' => 'Double', ], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
